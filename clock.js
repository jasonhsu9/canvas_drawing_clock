var c = document.getElementById('clock');
var ctx = c.getContext('2d');
console.log(ctx);
var width = ctx.canvas.width;
var height = ctx.canvas.height;
var r = width / 2;

var rem = width / 200; 
//rem确定比例，width变量的值变化（canvas容器尺寸变化）时，求出新width和原有尺寸200的比例（newWidth/200）
//整个时钟用到数值的地方，都乘以比例，此时简单修改canvas元素的（width/height值，就可以等比缩放整个时钟）
//比例关系可以在最后一步来完成

function drawBackground() {
    //由width="200px" height="200px"可知：canvas是一个正方形
    // 默认原点坐标是ctx的左上角
    //现在将原点重定义到正方形中心
    ctx.translate(r, r); //translate(num, num就是重新映射画布上的原点（0，0）
    ctx.beginPath();
    ctx.lineWidth = 10 * rem; //宽度是以默认值1px为基础，向两侧扩展
    ctx.arc(0, 0, r - ctx.lineWidth / 2, 0, 2 * Math.PI, true); //最后一个参数表示逆时针的布尔值
    //true为 使用 逆时针，false为相反
    //不管第六个参数使用 逆时针或者 顺时针，弧度的规则都是三点钟为0，顺时针逐渐增大
    //stroke() 或者 fill()----填充还是绘制
    ctx.stroke(); //默认宽度1px

    var hourNumbers = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2];
    ctx.font = 18 * rem + 'px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    hourNumbers.forEach(function (number, index) {
        var rad = 2 * Math.PI / 12 * index; //以3点钟（数字3）为0弧度，顺时针弧度数逐渐增大
        //每次增加 2 * Math.PI / 12 = Math.PI / 6 弧度

        var y = Math.sin(rad) * (r - 30 * rem);
        var x = Math.cos(rad) * (r - 30 * rem);

        ctx.fillText(number, x, y);
    });
    for (var i = 0; i < 60; i++) {
        var rad = 2 * Math.PI / 60 * i;
        var x = Math.cos(rad) * (r - 15 * rem);
        var y = Math.sin(rad) * (r - 15 * rem);

        ctx.beginPath();
        if (i % 5 === 0) {
            ctx.fillStyle = '#000';
        } else {
            ctx.fillStyle = '#ccc';
        }
        ctx.arc(x, y, 2 * rem, 0, 2 * Math.PI, false);
        ctx.fill();
    }
}


function drawHour(hour, minute) {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 6 * rem;
    ctx.lineCap = 'round';
    var hRad = 2 * Math.PI / 12 * hour;
    //时针旋转一个弧度，分针旋转720个弧度。当分钟为minute时，分针旋转了minute * 2 * Math.PI 的弧度值
    //此时时针旋转弧度值== 比例关系（1/720 * minute * 2 * Math.PI）
    var mRad = 1 / 720 * (2 * Math.PI * minute); //时针一小时rad 为：   2*Math.PI/12       分针一小时rad为： 2 * Math.PI * 60 两个相除为1/720
    var totalRad = hRad + mRad;
    ctx.rotate(totalRad);
    //画布要在绘制之前旋转
    ctx.moveTo(0, 10 * rem);
    ctx.lineTo(0, -r / 2);
    ctx.stroke();
    ctx.restore();
}

function drawMinute(minute) {
    ctx.save();
    var rad = 2 * Math.PI / 60 * minute;
    ctx.rotate(rad);
    ctx.beginPath();
    ctx.lineWidth = 3 * rem;
    ctx.lineCap = 'round';
    //画布要在绘制之前旋转
    ctx.moveTo(0, 10 * rem);
    ctx.lineTo(0, -r + 30 * rem);
    ctx.stroke();
    ctx.restore();
}

function drawSecond(second) {
    ctx.save();
    var rad = 2 * Math.PI / 60 * second;
    ctx.rotate(rad);
    ctx.beginPath();
    ctx.fillStyle = '#c14543';
    //画布要在绘制之前旋转
    ctx.moveTo(-2 * rem, 20 * rem);
    ctx.lineTo(2 * rem, 20 * rem);
    ctx.lineTo(1, -r + 18 * rem);
    ctx.lineTo(-1, -r + 18 * rem);
    ctx.fill();
    ctx.restore();
}

function drawDot() {
    ctx.beginPath();
    ctx.fillStyle = '#fff';
    ctx.arc(0, 0, 4 * rem, 0, Math.PI * 2, false);
    ctx.fill();
}





function drawAll() {
    ctx.save(); //drawBackground函数修改了原点，所以这里先保存当前画布环境
    ctx.clearRect(0, 0, width, height);

    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();

    drawBackground(); //
    drawHour(hour, minute);
    drawMinute(minute);
    drawSecond(second);
    drawDot()

    ctx.restore(); //由于原点被修改，时钟绘制完后还原 之前save()的画布环境，1秒后再次绘制时钟，如此循环
}

drawAll();
//页面打开立刻绘制时钟

var reDraw = setInterval(drawAll, 1000);
//然后每秒再绘制一次时钟